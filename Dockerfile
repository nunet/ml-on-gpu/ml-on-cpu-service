# Using the TensorFlow CPU image as base
FROM tensorflow/tensorflow:latest

# Create /workspace directory and add user "nunet" with home directory set to /workspace and login shell set to /bin/bash
RUN apt update && \
    apt -y upgrade && \
    mkdir /workspace && \
    useradd -d /workspace -s /bin/bash nunet && \
    chown nunet:nunet /workspace && \
    apt install -y --no-install-recommends apt-utils bandit && \
    apt clean && \
    rm -rf /var/lib/apt/lists/* && \
    sed -i 's/^ldconfig/# ldconfig/g' /etc/bash.bashrc && \
    pip install --upgrade pip setuptools wheel && \
    pip install --no-cache-dir pexpect scikit-learn && \
    pip install --no-cache-dir -f https://download.pytorch.org/whl/cpu/torch_stable.html torch torchvision && \
    rm -rf /root/.cache/pip/*

# Set home directory as /workspace and add dependencies directory to PATH
ENV HOME=/workspace PATH="/workspace/.local/bin:$PATH"

# Set /workspace as work directory
WORKDIR /workspace

# Copy required files
COPY prepare_ml.py nunet_logging.py ./

# Switch to user "nunet"
USER nunet

# Entrypoint for the container to prepare and run the ML Job on the onboarder's machine
ENTRYPOINT [ "python", "prepare_ml.py" ]

# url_link: URL where the model will be downloaded
# cmds_dependencies: list of dependencies to be installed by pip
CMD [ "$url_link", "$cmds_dependencies" ]
