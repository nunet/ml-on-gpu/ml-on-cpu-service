import logging
import sys

class InfoFilter(logging.Filter):
    def filter(self, record):
        return record.levelno in (logging.DEBUG, logging.INFO, logging.WARNING)

class ErrorWarningFilter(logging.Filter):
    def filter(self, record):
        return record.levelno in (logging.ERROR, logging.CRITICAL)

def configure_logging():
    # Create handlers
    info_handler = logging.StreamHandler(sys.stdout)
    error_warning_handler = logging.StreamHandler(sys.stderr)

    # Set the handler's level to DEBUG for info_handler
    info_handler.setLevel(logging.DEBUG)
    
    # Set the handler's level to ERROR
    error_warning_handler.setLevel(logging.ERROR)

    # Apply filters
    info_handler.addFilter(InfoFilter())
    error_warning_handler.addFilter(ErrorWarningFilter())

    # Create formatters and add them to handlers
    format = logging.Formatter('%(message)s')
    info_handler.setFormatter(format)
    error_warning_handler.setFormatter(format)

    # Get the root logger
    root_logger = logging.getLogger()

    # Set the root logger's level to DEBUG
    root_logger.setLevel(logging.DEBUG)

    # Add handlers to the root logger
    root_logger.addHandler(info_handler)
    root_logger.addHandler(error_warning_handler)